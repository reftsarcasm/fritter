/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.util.argumentresolvers
import java.util.logging.Level
import java.util.logging.Logger
import java.util.logging.Logging
import org.springframework.core.MethodParameter
import org.springframework.messaging.Message
import org.springframework.messaging.handler.invocation.HandlerMethodArgumentResolver
import org.springframework.security.core.Authentication
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.core.context.SecurityContextHolder

/** 
 * Argument resolver to inject the principal.getPrincipal() member into 
 * message mapping methods
 * @author ReftSarcasm
 */
class WebsocketAuthenticationPrincipalResolver extends HandlerMethodArgumentResolver {
  def resolveArgument(parameter : MethodParameter, message : Message[_]) : Object = {
    //http://docs.spring.io/spring-security/site/docs/current/reference/html/websocket.html
    //Apparently the SecurityContextHolder in the message headers
    var e = message.getHeaders().get("simpUser").asInstanceOf[Authentication];
    if(e == null){
      return null;
    }
    var u = e.getPrincipal();
    return u
  }
  
  def supportsParameter(parameter : MethodParameter) : Boolean = {
    for(a <- parameter.getParameterAnnotations()){
      if(a.isInstanceOf[AuthenticationPrincipal]){
        return true
      }
    }
    false
  }
  
}
