/*
 * Copyright (c) 2015, ReftSarcasm <reft@sarcasm.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.reftsarcasm.fritter.controllers
import org.springframework.security.access.intercept.RunAsUserToken
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.{RequestMapping, PathVariable, RequestMethod, ModelAttribute}
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.security.core.annotation.AuthenticationPrincipal
import com.reftsarcasm.forms.post.FritterPostForm
import com.reftsarcasm.fritter.models.FritterPost
import com.reftsarcasm.fritter.models.FritterUser
import com.reftsarcasm.fritter.repositories.PostCrudRepo
import java.security.Principal
import javax.inject.Inject

/**
 * @author ReftSarcasm <reft@sarcasm.net>
 */
@Controller
@RequestMapping(value = Array("/post"))
class PostController {
  
  @Inject var postRepo : PostCrudRepo = null
    
  @RequestMapping(Array("/{postId}"))
  def getPost(@PathVariable(value = "postId") postId : java.lang.Long,m : Model) = {
    var p = postRepo.findOne(postId);
    if(p == null){
      "posts/postNotFound";
    }else{
      m.addAttribute("post", p);
        
      "posts/viewPost";
    }
  }
    
  @RequestMapping(value = Array("/", ""), method = Array(RequestMethod.GET))
  def getPostForm(m : Model) = {
    var post = new FritterPostForm();
    m.addAttribute("postObject", post);
    "posts/postForm";
  }
  @RequestMapping(value = Array("/", ""), method = Array(RequestMethod.POST))
  def sendPostForm(m : Model, @AuthenticationPrincipal user : FritterUser,
                             @ModelAttribute("postObject") post : FritterPost) = {
    post.setPoster(user)
    postRepo.save(post)
    "posts/postForm"
  }
}
