/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.controllers
import com.reftsarcasm.fritter.models.FritterUser
import com.reftsarcasm.fritter.repositories.FritterFollowCrudRepo
import com.reftsarcasm.fritter.repositories.UserCrudRepo
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.{RequestMapping,PathVariable, ResponseBody}
import javax.inject.Inject
import scala.collection.JavaConverters._

@Controller
@RequestMapping(Array("/user"))
class UserController {
  @Inject var userRepo : UserCrudRepo = null
  @Inject var followRepo : FritterFollowCrudRepo = null
  
  @RequestMapping(Array("/{username}"))
  def getUserPage(@PathVariable(value = "username") username : java.lang.String, m : Model) = {
    val user = userRepo.findByUsername(username)
    m.addAttribute("user", user)
    "user/profile"
  }
}
