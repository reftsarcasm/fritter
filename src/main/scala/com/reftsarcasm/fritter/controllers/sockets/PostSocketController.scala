/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.controllers.sockets
import org.springframework.stereotype.Controller
import com.reftsarcasm.fritter.models.FritterPost
import com.reftsarcasm.fritter.models.FritterUser
import com.reftsarcasm.fritter.repositories.PostCrudRepo
import org.springframework.messaging.handler.annotation.{SendTo,MessageMapping}
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.messaging.simp.annotation.{SubscribeMapping}
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.transaction.annotation.Transactional
import java.util.logging.Level
import java.util.logging.Logger
import javax.inject.Inject

@Controller
class PostSocketController {
  @Inject private var postRepo : PostCrudRepo = null;
  @Inject private var simpTemplate : SimpMessagingTemplate = null;
  
  @MessageMapping(Array("/post/send"))
  @SendTo(Array("/timeline/incoming"))
  def createPost(@AuthenticationPrincipal user : FritterUser, post : FritterPost) : FritterPost = {
    post.setPoster(user)
    //Crashes, there's no db context when doing this from a websocket
    simpTemplate.convertAndSend("/timeline/incoming/" + user.getId().toString, post)
    try{
      return postRepo.save(post)
    }catch{
      case e : Exception => {
        Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getClass().getName())
        Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage())
        throw e
      }
    }
  }
}
