/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.controllers.rest

import com.reftsarcasm.fritter.repositories.FritterFollowCrudRepo
import org.springframework.stereotype.Controller
import com.reftsarcasm.fritter.models.FritterFollow
import com.reftsarcasm.fritter.models.FritterUser
import org.springframework.web.bind.annotation.{RequestMapping, ResponseBody, RequestMethod, PathVariable}
import org.springframework.security.core.annotation.AuthenticationPrincipal
import com.reftsarcasm.fritter.repositories.UserCrudRepo
import javax.inject.Inject
import scala.collection.JavaConverters._

@Controller
@RequestMapping(value = Array("/follow"))
class FollowController {
  @Inject private var followRepo : FritterFollowCrudRepo = null;
  @Inject private var userRepo : UserCrudRepo = null;
  
  @RequestMapping(value = Array("/following"), method = Array(RequestMethod.GET))
  @ResponseBody
  def getUserFollowers(@AuthenticationPrincipal user : FritterUser) = {
    followRepo.findByFollower(user).asScala.map(a => a.getFollowed().getId()).asJava
  }
  
  @RequestMapping(value = Array("/{followId}"), method = Array(RequestMethod.PUT))
  @ResponseBody
  def followUser(@AuthenticationPrincipal user : FritterUser, @PathVariable(value = "followId") id : java.lang.Long) = {
    var toFollow = userRepo.findOne(id)
    var follow = new FritterFollow();
    follow.setFollowed(toFollow)
    follow.setFollower(user)
    followRepo.save(follow)
  }
}
