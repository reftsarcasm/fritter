/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.controllers.rest
import org.springframework.web.bind.annotation.{RestController,RequestMapping,RequestMethod, PathVariable, RequestBody, ResponseBody}
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.stereotype.Controller
import org.springframework.messaging.handler.annotation.SendTo
import com.reftsarcasm.fritter.exceptions.PostNotFoundException
import com.reftsarcasm.fritter.models.FritterPost
import com.reftsarcasm.fritter.models.FritterUser
import com.reftsarcasm.fritter.repositories.PostCrudRepo
import java.util.logging.Level
import java.util.logging.Logger
import javax.inject.Inject

@Controller
@RequestMapping(Array("/rest/post"))
class PostRestController {
  @Inject private var postRepo : PostCrudRepo = null;
  @Inject private var simpMessaging : SimpMessagingTemplate = null;
  
  @RequestMapping(value = Array("/{postId}"), method = Array(RequestMethod.GET))
  @ResponseBody
  def getPost(@AuthenticationPrincipal user : FritterUser, @PathVariable(value = "postId") postId : Long)  = {
    var post = postRepo.findOne(postId);
    if(post == null){
      throw new PostNotFoundException();
    }
    post
  };
  
  @RequestMapping(value = Array("","/"), method = Array(RequestMethod.PUT))
  @ResponseBody
  def createPost(@AuthenticationPrincipal user : FritterUser, @RequestBody post : FritterPost) : FritterPost = {
    post.setPoster(user)
    var p = postRepo.save(post)
    simpMessaging.convertAndSend("/timeline/incoming/"+user.getId(), p)
    p
  }
}
