/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.controllers
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}
import com.reftsarcasm.fritter.repositories.PostCrudRepo
import com.reftsarcasm.fritter.repositories.UserCrudRepo
import javax.inject.Inject

@Controller
@RequestMapping(Array("/search"))
class SearchController {
  @Inject
  private var postRepo : PostCrudRepo = null
  @Inject
  private var userRepo : UserCrudRepo = null
  
  @RequestMapping(value = Array("/"), method = Array(RequestMethod.GET))
  def doSearch(searchTerm : String, model : Model) : String = {
    
    "search/searchResults"
  }
}
