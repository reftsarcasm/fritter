/*
 * Copyright (c) 2015, ReftSarcasm <reft@sarcasm.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.reftsarcasm.fritter.controllers

import com.reftsarcasm.fritter.models.FritterPost
import com.reftsarcasm.fritter.models.FritterUser
import com.reftsarcasm.fritter.repositories.FritterFollowCrudRepo
import com.reftsarcasm.fritter.repositories.PostCrudRepo
import java.util.ArrayList
import java.util.logging.Level
import java.util.logging.Logger
import javax.inject.Inject
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, PathVariable}
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.RestController
import scala.collection.JavaConverters._

/**
 * Controller for timelines
 * @author ReftSarcasm <reft@sarcasm.net>
 */
@RestController
@RequestMapping(value = Array("/timeline"))
class TimelineController {
  @Inject private var postRepo : PostCrudRepo = null
  @Inject private var followRepo : FritterFollowCrudRepo = null
    
  @RequestMapping(value = Array("","/"), method = Array(RequestMethod.GET))
  def getPosts(@AuthenticationPrincipal user : FritterUser) = { 
    getPostsWithOffset(user, 0)
  }
  
  @RequestMapping(value = Array("/{offset}"), method = Array(RequestMethod.GET))
  def getPostsWithOffset(@AuthenticationPrincipal user : FritterUser, @PathVariable("offset") offset : Integer) = {
    if(user == null)
      new ArrayList[FritterPost]
    else{
      var follows = followRepo.findByFollower(user).asScala.map(x => x.getFollowed())
      var pageable = new PageRequest(offset,10)
      postRepo.findByPosterInOrderByIdDesc(follows.asJavaCollection,pageable)
    }
  }
  
  @RequestMapping(value = Array("/latest/{lastId}"), method = Array(RequestMethod.GET))
  def getLatestPosts(@AuthenticationPrincipal user : FritterUser, @PathVariable("lastId") lastId : Long) ={
    if(user == null)
      new ArrayList[FritterPost]
    else{
      var follows = followRepo.findByFollower(user).asScala.map(x => x.getFollowed())
      postRepo.getLatestTimeline(follows.asJavaCollection, lastId)
    }
  }
  
  @RequestMapping(value = Array("/user/{id}"), method = Array(RequestMethod.GET))
  def getUserTimeline(@PathVariable("id") userId : Long) = {
    Logger.getLogger(getClass().getName()).log(Level.SEVERE, s"In user timeline for user ID $userId")
    postRepo.getByUserId(userId)
  }
}

