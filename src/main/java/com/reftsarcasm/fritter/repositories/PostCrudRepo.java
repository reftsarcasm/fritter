/*
 * Copyright (c) 2015, ReftSarcasm <reft@sarcasm.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.reftsarcasm.fritter.repositories;

import com.reftsarcasm.fritter.models.FritterPost;
import com.reftsarcasm.fritter.models.FritterUser;
import java.util.Collection;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ReftSarcasm <reft@sarcasm.net>
 */
public interface PostCrudRepo extends PagingAndSortingRepository<FritterPost, Long> {
    /**
     * Get the user time line
     * @param following The users to get the posts from
     * @return List of posts by these users
     */
    List<FritterPost> findByPosterInOrderByIdDesc(Collection<FritterUser> following);
    
    /**
     * Get the user's time line
     * @param following The users that are being followed
     * @param page The page to get
     * @return The list of posts
     */
    List<FritterPost> findByPosterInOrderByIdDesc(Collection<FritterUser> following, Pageable page);
    
    /**
     * Get the user's time line
     * @param following The users being followed
     * @param page Page offset
     * @param id Last id seen
     * @return The newest time line posts
     */
    List<FritterPost> findByPosterInAndIdGreaterThanOrderByIdDesc(Collection<FritterUser> following, Long id, Pageable page);
    /**
     * Get the user's time line
     * @param following The users being followed
     * @param id Last id seen
     * @return The newest time line posts
     */
    @Query("select p from #{#entityName} p where p.id > ?#{#id} and p.poster in ?#{#following}")
    List<FritterPost> getLatestTimeline(@Param("following") Collection<FritterUser> following, 
            @Param("id") long id);    
    
    /**
     * Get a user's latest posts
     * @param id ID of the user to look for
     * @return The user's posts
     */
    @Query("select p from #{#entityName} p where p.poster.id = ?#{#userId}")
    List<FritterPost> getByUserId(@Param("userId") Long userId);
}
