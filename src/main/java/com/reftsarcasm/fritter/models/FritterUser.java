/*
 * Copyright (c) 2015, ReftSarcasm <reft@sarcasm.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.reftsarcasm.fritter.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author ReftSarcasm <reft@sarcasm.net>
 */
@Entity
@Table(name = "fritter_users")
public class FritterUser implements Serializable, UserDetails {
    private String username;
    private String displayName;
    private Date joinTime;
    private Long id;
    private Set<FritterRole> roles;
    private Boolean isLocked;

    @Column(name = "is_locked", updatable = true, nullable = false)
    @GeneratedValue
    @JsonIgnore
    public Boolean getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(Boolean isLocked) {
        this.isLocked = isLocked;
    }
    
    public FritterUser(){
        roles = new HashSet<FritterRole>();
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonIgnore
    public Set<FritterRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<FritterRole> roles) {
        this.roles = roles;
    }
    
    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "username", length = 15, updatable = true, nullable = false, unique = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    @Column(name = "display_name", length = 50, updatable = true, nullable = false)
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    
    protected void prePersist(){
        if(getJoinTime() == null)
            setJoinTime(new Date());
    }

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @GeneratedValue
    @Column(name = "join_date", updatable = false, nullable = false)
    public Date getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Date postTime) {
        this.joinTime = postTime;
    }
    
    private String passhash;

    @Column(name = "password_hash", updatable = true, nullable = false, length = 256)
    @JsonIgnore
    public String getPasshash() {
        return passhash;
    }

    public void setPasshash(String passhash) {
        this.passhash = passhash;
    }

    @Override
    @Transient
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }

    @Override
    @Transient
    @JsonIgnore
    public String getPassword() {
        return getPasshash();
    }

    @Override
    @Transient
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return getIsLocked();
    }

    @Override
    @Transient
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return !getIsLocked();
    }

    @Override
    @Transient
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return getIsLocked();
    }

    @Override
    @Transient
    @JsonIgnore
    public boolean isEnabled() {
        return getIsLocked();
    }
}
