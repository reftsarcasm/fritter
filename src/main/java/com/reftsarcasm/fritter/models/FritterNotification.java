/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.models;

import java.util.Date;

/**
 * Notification object for transmission over websockets
 * @author ReftSarcasm <reft@sarcasm.net>
 * @param <T> Payload type
 */
public class FritterNotification<T> {
    private T payload;
    private Date time;
    
    public FritterNotification(T payload){
        this.time = new Date();
        this.payload = payload;
    }

    /**
     * Time that this notification was generated
     * @return The generation time
     */
    public Date getTime() {
        return time;
    }

    /**
     * Payload of this notification. Can be anything.
     * @return The included object
     */
    public T getPayload() {
        return payload;
    }
}
