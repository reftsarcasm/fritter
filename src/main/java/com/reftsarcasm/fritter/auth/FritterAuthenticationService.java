/*
 * Copyright (c) 2015, ReftSarcasm <reft@sarcasm.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.reftsarcasm.fritter.auth;

import com.reftsarcasm.fritter.models.FritterRole;
import com.reftsarcasm.fritter.models.FritterUser;
import com.reftsarcasm.fritter.repositories.UserCrudRepo;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import org.springframework.security.access.intercept.RunAsUserToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 *
 * @author ReftSarcasm <reft@sarcasm.net>
 */
@Component
public class FritterAuthenticationService implements AuthenticationProvider {

    @Inject
    private UserCrudRepo userRepo;
    @Inject
    private PasswordEncoder passEncoder;

    @Override
    public Authentication authenticate(Authentication a) throws AuthenticationException {
        String name = a.getName();
        final Object credentials = a.getCredentials();
        String password = credentials.toString();
        final FritterUser user = userRepo.findByUsername(name);
        if (user == null) {
            throw new AuthenticationException("Could not authenticate user!") {
            };
        }
        String retrieved = user.getPasshash();
        if (!passEncoder.matches(password, retrieved)) {
            throw new AuthenticationException("Could not authenticate user!") {
            };
        }
        return new RunAsUserToken(name, user, credentials, user.getRoles(), a.getClass());

    }

    @Override
    public boolean supports(Class<?> type) {
        return type.equals(UsernamePasswordAuthenticationToken.class);
    }
}
