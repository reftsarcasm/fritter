/* 
 * Copyright (c) 2015, ReftSarcasm <reft@sarcasm.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
var timelineApp = angular.module('timelineApp', ['ngStomp','angularMoment']);

timelineApp.config([
    "$httpProvider", function ($httpProvider) {
        $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=_csrf]').attr('content');
    }
]);

timelineApp.controller('fritterApp', function ($scope, $http, $stomp, $timeout) {
    var timelineGet = $('#timeline_url').attr('href');
    var postUrl = $('#post_url').attr('href');
    var getLatest = $('#latest_url').attr('href');
    var rtTimeline = $('#stomp_endpoint').attr('href');
    var csrfToken = $('meta[name=_csrf]').attr('content');
    var following = $('#user_following').attr('href');
    var doFollowUrl = $('#do_follow').attr('href');
    $scope.rootUrl = $('#root_url').attr('href');
    $scope.headers = {
        'X-CSRF-Token': csrfToken
    };
    $http.get(timelineGet).success(function (data) {
        $scope.inputViewModel.posts = data;
    });
    $scope.inputViewModel = {
        postValue: '',
        status: '',
        pending: [],
        posts: [],
        following: {}
    };
    $scope.putPost = function () {
        $http.put(postUrl, {content: $scope.inputViewModel.postValue})
                .success(function (data, status, headers, config) {
                    $scope.inputViewModel.postValue = '';
                })
                .error(function (data, status, headers, config) {
                });
    };
    $scope.movePendingToPosts = function () {
        var pend = $scope.inputViewModel.pending;
        var posts = $scope.inputViewModel.posts;
        while (pend.length > 0) {
            var cur = pend.shift();
            if (cur.id <= posts[0].id) {
                continue;
            }
            posts.unshift(cur);
        }
    };
    $scope.addToPending = function (data) {
        var len = data.length;
        for (var i = 0; i < len; i++) {
            var cur = data[i];
            if ($scope.inputViewModel.pending.length !== 0 && cur.id <= $scope.inputViewModel.pending[$scope.inputViewModel.pending.length - 1].id) {
                continue;
            }
            $scope.inputViewModel.pending.push(cur);
        }
        $scope.$apply();
    };
    $stomp.connect(rtTimeline, $scope.headers)
            .then(function (frame) {
                console.log(following);
                $http.get(following).success(function (data, status, headers, config) {
                    var idx = data.length;
                    while (idx--) {
                        $scope.subscribeTo(idx);
                    }
                }).
                        error(function (data, status, headers, config) {
                        });
            });
    $scope.receivePost = function (payload, headers, res) {
        $scope.addToPending([payload]);
    }

    $scope.subscribeTo = function (idx) {
        var surl = '/timeline/incoming/' + idx;
        $stomp.subscribe(surl, $scope.receivePost);
        $scope.inputViewModel.following[idx] = true;
    }

    $scope.followUser = function (id) {
        $http.put(doFollowUrl + id, {})
                .success(function (data, status, headers, config) {
                    $scope.subscribeTo(id);
                })
                .failure();
    }
        
    $scope.unfollowUser = function (id) {
        console.log("Unfollowed "+ id);
//        $http.put(doFollowUrl + id, {})
//                .success(function (data, status, headers, config) {
//                    $scope.subscribeTo(id);
//                })
//                .failure();
    }

});
timelineApp.controller('userProfileApp', function($scope, $http, $stomp){
    var postUrl = $('#post_url').attr('href');
    var rtTimeline = $('#stomp_endpoint').attr('href');
    var csrfToken = $('meta[name=_csrf]').attr('content');
    var profileId = $('meta[name=profile-id]').attr('content');
    var timelineGet = $('#profile_url').attr('href')+profileId;
    
    $scope.inputViewModel = {
        'posts' : [],
        'pending' : []
    };
    
    
    $scope.movePendingToPosts = function () {
        var pend = $scope.viewModel.pending;
        var posts = $scope.viewModel.posts;
        while (pend.length > 0) {
            var cur = pend.shift();
            if (cur.id <= posts[0].id) {
                continue;
            }
            posts.unshift(cur);
        }
    };
    

    $scope.subscribeTo = function (idx) {
        var surl = '/timeline/incoming/' + idx;
        $stomp.subscribe(surl, $scope.receivePost);
    }
    $scope.addToPending = function (data) {
        var len = data.length;
        for (var i = 0; i < len; i++) {
            var cur = data[i];
            if ($scope.viewModel.pending.length !== 0 && cur.id <= $scope.inputViewModel.pending[$scope.inputViewModel.pending.length - 1].id) {
                continue;
            }
            $scope.viewModel.pending.push(cur);
        }
        $scope.$apply();
    };
    
    $http.get(timelineGet).success(function (data) {
        $scope.inputViewModel.posts = data;
    });   
    $stomp.connect(rtTimeline, $scope.headers)
            .then(function (frame) {
                $scope.subscribeTo(profileId);
            });
});