/*
 * Copyright (c) 2015, ReftSarcasm <reft@sarcasm.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.reftsarcasm.fritter.controllers;

import com.reftsarcasm.fritter.models.FritterPost;
import com.reftsarcasm.fritter.models.FritterUser;
import com.reftsarcasm.fritter.repositories.PostCrudRepo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.ui.Model;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.security.access.intercept.RunAsUserToken;
import org.springframework.test.util.ReflectionTestUtils;
import static scala.collection.JavaConversions.*;

/**
 *
 * @author ReftSarcasm <reft@sarcasm.net>
 */
public class PostControllerTest {
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testViewPostFound(){
        Model m = mock(Model.class);
        FritterPost p = mock(FritterPost.class);
        PostCrudRepo pr = mock(PostCrudRepo.class);
        when(pr.findOne(1l)).thenReturn(p);
        PostController controller = new PostController();
        ReflectionTestUtils.setField(controller, "postRepo", pr);
        final String expectedView = "posts/viewPost";
        String returnedView = controller.getPost(Long.valueOf(1l), m);
        assertEquals(expectedView, returnedView);
        verify(m).addAttribute("post", p);
    }
    
    @Test
    public void testViewPostNotFound(){
        Model m = mock(Model.class);
        PostCrudRepo pr = mock(PostCrudRepo.class);
        when(pr.findOne(1l)).thenReturn(null);
        PostController controller = new PostController();
        ReflectionTestUtils.setField(controller, "postRepo", pr);
        final String expectedView = "posts/postNotFound";
        String returnedView = controller.getPost(Long.valueOf(1l), m);
        assertEquals(expectedView, returnedView);
    }
    
    @Test
    public void testPostPost(){
        Model m = mock(Model.class);
        PostCrudRepo pr = mock(PostCrudRepo.class);
        FritterPost postMock = mock(FritterPost.class);
        RunAsUserToken token = mock(RunAsUserToken.class);
        FritterUser userMock = mock(FritterUser.class);
        when(token.getPrincipal()).thenReturn(userMock);
        PostController controller = new PostController();
        ReflectionTestUtils.setField(controller, "postRepo", pr);
        controller.sendPostForm(m, userMock, postMock);
        verify(postMock).setPoster(userMock);
        verify(pr).save(postMock);
    }
}
