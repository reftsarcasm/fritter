/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.controllers

import org.junit.{Test, Before};
import com.reftsarcasm.fritter.models.{FritterPost,FritterUser}
import com.reftsarcasm.fritter.repositories.PostCrudRepo
import com.reftsarcasm.fritter.controllers.rest.PostRestController
import java.security.Principal
import org.junit.Assert._
import org.mockito.Mockito._
import org.mockito.{Mock, InjectMocks}
import org.mockito.MockitoAnnotations.initMocks
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.test.util.ReflectionTestUtils

class RestPostControllerTest {
  @Mock
  private var p : FritterPost = null 
  @Mock
  private var pr : PostCrudRepo = null
  @Mock
  private var principal : Principal = null
  @Mock
  private var user : FritterUser = null
  @Mock
  private var simpMessaging : SimpMessagingTemplate = null
  @InjectMocks
  private var controller : PostRestController = null
  
  @Before
  def doBefore() : Unit ={
    initMocks(this)
  }
  
  @Test
  def getPost() : Unit = {
    when(user.getId()).thenReturn(1)
    when(pr.findOne(1)).thenReturn(p);
    var ret = controller.getPost(user, 1)
    verify(pr).findOne(1)
    assertSame(p, ret)
  }
  
  @Test
  def putPost() : Unit = {
    when(user.getId()).thenReturn(1)
    when(pr.findOne(1)).thenReturn(p);
    when(pr.save(p)).thenReturn(p);
    var ret = controller.createPost(user, p)
    verify(p).setPoster(user)
    verify(pr).save(p)
    assertSame(p, ret)
    verify(simpMessaging).convertAndSend("/timeline/incoming/1", p)
  }
}
