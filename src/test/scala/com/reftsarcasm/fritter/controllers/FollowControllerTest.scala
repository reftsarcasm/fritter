/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.controllers
import org.junit.{Test,Before};
import org.mockito.ArgumentCaptor
import org.mockito.Matchers._
import com.reftsarcasm.fritter.controllers.rest.FollowController
import com.reftsarcasm.fritter.models.FritterFollow
import com.reftsarcasm.fritter.models.FritterUser
import com.reftsarcasm.fritter.repositories.FritterFollowCrudRepo
import com.reftsarcasm.fritter.repositories.UserCrudRepo
import java.util.ArrayList
import org.junit.Assert._
import org.mockito.Mockito._
import org.mockito.MockitoAnnotations.initMocks;
import org.mockito.{Mock, InjectMocks}
import org.springframework.test.util.ReflectionTestUtils

class FollowControllerTest {
  
  @Mock
  private var followMock : FritterFollowCrudRepo = null 
  
  @Mock
  private var fMock : FritterFollow = null
  
  @Mock
  private var userMock : FritterUser = null
  
  @Mock
  private var userRepoMock : UserCrudRepo = null
  
  @InjectMocks
  private var controller : FollowController = null
  
  @Before
  def doBefore() : Unit = {
    initMocks(this)
  }
  
  @Test
  def testGetFollowers() : Unit = {
    when(fMock.getFollowed()).thenReturn(userMock)
    when(userMock.getId()).thenReturn(1)
    var ulist = new ArrayList[FritterFollow]();
    ulist.add(fMock)
    when(followMock.findByFollower(userMock)).thenReturn(ulist)
    
    var followed = controller.getUserFollowers(userMock)
    assertEquals(followed.size, 1)
    assertTrue(followed.get(0) == 1)
  }
  
  @Test
  def testFollow() : Unit = {
   when(userRepoMock.findOne(1)).thenReturn(userMock)
    
    controller.followUser(userMock, 1)
    var arg = ArgumentCaptor.forClass(classOf[FritterFollow])
    verify(followMock).save(arg.capture())
    verify(userRepoMock).findOne(1)
    assertSame(arg.getValue().getFollowed(), userMock)
    assertSame(arg.getValue().getFollower(), userMock)
  }
}
