/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.controllers

import com.reftsarcasm.fritter.models.FritterFollow
import com.reftsarcasm.fritter.models.FritterPost
import com.reftsarcasm.fritter.models.FritterUser
import com.reftsarcasm.fritter.repositories.FritterFollowCrudRepo
import com.reftsarcasm.fritter.repositories.PostCrudRepo
import com.reftsarcasm.fritter.repositories.UserCrudRepo
import org.springframework.data.domain.Pageable
import org.springframework.security.access.intercept.RunAsUserToken
import org.mockito.Matchers
import org.mockito.{Mock,InjectMocks}
import org.mockito.MockitoAnnotations.initMocks
import org.mockito.Matchers._
import org.mockito.Mockito._
import java.util.ArrayList
import org.junit._
import org.junit.Assert._

class ScalaTimelineControllerTest {
  @Mock
  private var userMock : FritterUser = null;
  @Mock
  private var followMock : FritterFollow = null;
  @Mock
  private var principalMock : RunAsUserToken = null;
  @Mock
  private var userRepoMock : UserCrudRepo = null;
  @Mock
  private var postRepoMock : PostCrudRepo = null;
  @Mock
  private var followRepoMock : FritterFollowCrudRepo = null;
  @InjectMocks
  private var controller : TimelineController = null;
    
  @Before
  def setUp() = {
    initMocks(this)
  }
    
  @Test
  def testGetTimelineWithUser() : Unit = {
    var follows = new ArrayList[FritterFollow];
    follows.add(followMock);
    when(followRepoMock.findByFollower(userMock)).thenReturn(follows);
    when(followMock.getFollowed()).thenReturn(userMock)
    val posts = new ArrayList[FritterPost];
    when(postRepoMock
      .findByPosterInOrderByIdDesc(Matchers.any(classOf[java.util.List[FritterUser]]), Matchers.any(classOf[org.springframework.data.domain.Pageable])))
      .thenReturn(posts);
    val ret = controller.getPosts(userMock);
    verify(followRepoMock).findByFollower(userMock);
    verify(postRepoMock).findByPosterInOrderByIdDesc(Matchers.any(classOf[java.util.List[FritterUser]]), Matchers.any(classOf[org.springframework.data.domain.Pageable]));
  }
  @Test
  def testGetTimelineNoUser() = {
    val ret = controller.getPosts(mock(classOf[FritterUser]));
    assertEquals(0, ret.size);
  }
  
  @Test
  def testGetTimelineLatest() : Unit = {
    
    var follows = new ArrayList[FritterFollow];
    follows.add(followMock);
    when(followRepoMock.findByFollower(userMock)).thenReturn(follows);
    when(followMock.getFollowed()).thenReturn(userMock)
    val posts = new ArrayList[FritterPost];
    when(postRepoMock.getLatestTimeline(Matchers.any(classOf[java.util.List[FritterUser]]), Matchers.eq(600l)))
      .thenReturn(posts);
    val ret = controller.getLatestPosts(userMock, 600l);
    verify(followRepoMock).findByFollower(userMock);
    verify(postRepoMock).getLatestTimeline(Matchers.any(classOf[java.util.List[FritterUser]]), Matchers.eq(600l));
  }
  
  @Test
  def testGetUserTimeline() : Unit = {
    val expected = new java.util.ArrayList[FritterPost]
    when(postRepoMock.getByUserId(1)).thenReturn(expected)
    val ret = controller.getUserTimeline(1)
    assertSame(expected, ret)
    verify(postRepoMock).getByUserId(1)
  }
}
