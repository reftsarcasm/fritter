/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.controllers

import org.junit.{Test, Before};
import org.junit.Assert._
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers._;

class IndexControllerTest {
  private var mockMvc : MockMvc = null  
  
  @Before
  def doBefore : Unit = {
    mockMvc = MockMvcBuilders.standaloneSetup(new IndexController).build()
  }
  @Test
  def getIndexTest() : Unit = {
    mockMvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.TEXT_HTML_VALUE))
          .andExpect(status().isOk())
  }
}
