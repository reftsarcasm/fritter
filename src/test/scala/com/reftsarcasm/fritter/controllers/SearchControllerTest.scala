/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.controllers
import org.junit.{Test,Before}
import com.reftsarcasm.fritter.repositories.PostCrudRepo
import com.reftsarcasm.fritter.repositories.UserCrudRepo
import org.junit.Assert._
import org.mockito.Mockito._
import org.mockito.{Mock, InjectMocks}
import org.mockito.MockitoAnnotations.initMocks
import org.springframework.test.util.ReflectionTestUtils
import org.springframework.ui.Model

class SearchControllerTest {
  @InjectMocks
  private var controller : SearchController = null
  @Mock
  private var postRepo : PostCrudRepo = null
  @Mock
  private var userRepo : UserCrudRepo = null
  @Mock
  private var model : Model = null
  
  @Before
  def setup() : Unit = {
    initMocks(this)
  }
  
  @Test
  def testCorrectView() : Unit = {
    assertEquals("search/searchResults",controller.doSearch("searchTerm", model))
  }
  
  @Test
  def testEnsureSearchHitsReport : Unit = {
    val searchTerm = "searchTerm"
    
  }
}
