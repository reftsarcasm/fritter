/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.controllers

import org.junit.Test;
import com.reftsarcasm.fritter.controllers.sockets.PostSocketController
import com.reftsarcasm.fritter.models.FritterPost
import com.reftsarcasm.fritter.models.FritterUser
import com.reftsarcasm.fritter.repositories.PostCrudRepo
import java.security.Principal
import org.junit.Assert._
import org.mockito.Mockito._
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.test.util.ReflectionTestUtils

class SocketPostControllerTest {
  
  @Test
  def putPost() : Unit = {
    var p = mock(classOf[FritterPost])
    var pr = mock(classOf[PostCrudRepo])
    var principal = mock(classOf[Principal])
    var user = mock(classOf[FritterUser])
    var simpTemplate = mock(classOf[SimpMessagingTemplate])
    when(user.getUsername()).thenReturn("testUser");
    when(user.getId()).thenReturn(1)
    when(pr.save(p)).thenReturn(p);
    when(p.getContent()).thenReturn("test post");
    var controller = new PostSocketController();
    ReflectionTestUtils.setField(controller, "postRepo", pr);
    ReflectionTestUtils.setField(controller, "simpTemplate", simpTemplate);
    var ret = controller.createPost(user, p)
    verify(p).setPoster(user)
    verify(pr).save(p)
    verify(simpTemplate).convertAndSend("/timeline/incoming/1", p)
    assertSame(p, ret)
  }
}
