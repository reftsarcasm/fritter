/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.reftsarcasm.fritter.controllers
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.boot.test.{IntegrationTest,SpringApplicationConfiguration}
import org.junit.{Before,Test}
import org.junit.runner.RunWith
import org.springframework.test.context.web.WebAppConfiguration

@RunWith(classOf[SpringJUnit4ClassRunner])
@SpringApplicationConfiguration(locations = Array("classpath:/com/reftsarcasm/fritter/spring/fritter-servlet.xml"))
@WebAppConfiguration
@IntegrationTest(Array("server.port=0"))
class IndexControllerIntegrationTest {
  
}
