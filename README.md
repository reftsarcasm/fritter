[![Build Status](https://api.shippable.com/projects/55c002f5edd7f2c052938ac4/badge/master)](https://app.shippable.com/projects/55c002f5edd7f2c052938ac4)
Currently only half built, this is a hobby project, so it will not move very quickly.

Feel free to contribute, but be warned, I probably won't care too much and may rib you in the pull requests if I think your code is bad.

## Building
Fire up a console, cd to the project root, and type 
```
./gradlew build
```

## Running
This is a multi step process right now. Hoping to modify it to make it easier, but currently you need to do three different things:
* Edit gradle.properties to contain your DB info (should be able to use anything JPA can sit on, I use MySQL as you can probably tell)
* Edit src/main/resources/internal-config.properties to use the same DB

Then, run the following:

```
./gradlew build update appRun
```

This should bring Jetty up on localhost at port 8080

Good luck, you're going to need it
